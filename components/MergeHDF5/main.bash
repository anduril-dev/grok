
#!/bin/bash
# running functions.sh also sets logfile and errorfile.
source "$ANDURIL_HOME/bash/functions.sh"
export_command
tempdir=$( gettempdir ) # nice to have it always since it's the scratch disk

#/* Utility
# * Merges hdf5 files residing in a array to optOut1.
# * If they "overlap", will fail, at least as far as I've
# * tested it. So they are assumed to be independent.
# * Purpose is to join results after parallelization.
# */

IFS=$'\n'
keys=( $( getarraykeys array ) ) # $input_array (needed for script scraping)
files=( $( getarrayfiles array ) )

# First copy one of the files as the destination, because
# h5merge operates on two files one of which is the target

cp ${files[0]} $output_merged

for (( i=1; i<${#keys[@]};i++ ))
do
    echo Merging Index: $i Key: ${keys[$i]} File: $( basename ${files[$i]} ) to: $output_merged
    ls -l ${files[$i]}
    h5merge -i "${files[$i]}" -o $output_merged
done

# ls -l @optOut1@




#!/usr/bin/env python
import anduril
from anduril.arrayio import *
from anduril.args import *
args=anduril.args

import os, errno, time, optparse, logging, sys, re, collections, heapq, bisect, copy, pprint, random, math, itertools, csv, pdb, grok, numpy, scipy

#/* Preprocessing
# * Split GTF file into chromosomes. Of course, this should
# * be done with a generic component.
# */
os.makedirs(output_folder)

n_chromos=22
chromos=[str(i) for i in range(1,n_chromos+1)] + ["chr"+str(i) for i in range(1,n_chromos+1)] + ["X","Y","MT"] + ["chrX","chrY","chrMT"]

writers={}
annotation=grok.reader(input_annotation)
for reg in annotation:
    value=reg["seqid"]
    output=os.path.join(output_folder, value + ".gff")
    if not value in chromos:
        continue
    if not value in writers:
        writers[value]=annotation.writer(output)
    writer=writers[value]
    writer.add(reg)


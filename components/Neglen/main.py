
#!/usr/bin/env python
import anduril
from anduril.arrayio import *
from anduril.args import *
args=anduril.args

import os, errno, time, optparse, logging, sys, re, collections, heapq, bisect, copy, pprint, random, math, itertools, csv, pdb, grok, numpy, scipy

'''
/* Preprocessing
 *
 * We are only interested in rows that have a corresponding mate row with a negative template
 * length, BAM's tlen field.
 *
 * That means both the read and its mate were mapped successfully to the same chromosome.
 *
 * The read position can later be deduced from the mate and the template length as
 * read position = mate position + mate length - template length
 * 
 * In principle, should check the flag 0x40 for "first segment" i.e. read and 0x80 for "last segment" i.e. mate. This is done with the 0x40/0x80 flag checks.
 * FIXME: Still check if that m_left =r_pnext holds true even if read/mate are reversed
*/
'''

import itertools
from itertools import * 

sorted=grok.reader(input_sorted)
paired=sorted.writer(output_paired,"bam")

ID=None
reads=[]
mates=[]

total_regions=0
total_paired=0

double_mates=0
# Adding reads is triggered when the region ID is switched
# The dummy region is to stop the loop gracefully

# JoinMates
for region in chain(sorted,[{'ID':None, 'tlen':0}]):
    total_regions+=1
    region['Tags']=None # These may cause problems at the moment
    if not region['ID']==ID:

        # Try to match each mate
        for m in mates:
            added=False
            for r in reads:
                if r['tlen']==-m['tlen'] and m['left'] == r['pnext']-1 and (r['flag']&0x40) == (m['flag'] & 0x80):
                    if added:
                    #    raise Exception("More than one read for a mate! %r"%((reads,m), ))
                    #     print "More than one read for a mate! %r"%((reads,m), )
                        paired.add(m) # Add mate already here so we get pairwise data
                        total_paired+=1
                        double_mates+=1
                    total_paired+=1
                    paired.add(r) 
                    added=True

#                if not added:
#                    raise Exception("Could not pair a mate!")
#                    print "Could not pair a mate!"
#        print "Added!"
            if added:    
                total_paired+=1
                paired.add(m)

        mates=[]
        reads=[]
        ID=region["ID"]

    # In BAM files, the rightmost segment in a template has a negative template length
    if region['tlen']<0:
        mates.append(region)
    else:
        reads.append(region)
print "Number of double mates", double_mates
print "Total reads", total_regions
print "Total in paired", total_paired
print "Percentage", total_paired/float(total_regions)




#!/usr/bin/env python
import anduril
from anduril.arrayio import *
from anduril.args import *
args=anduril.args

import os, errno, time, optparse, logging, sys, re, collections, heapq, bisect, copy, pprint, random, math, itertools, csv, pdb, grok, numpy, scipy

'''
/* Preprocessing
 * From GTF file, find lengths of 5' and 3' UTR regions for each transcript.
 * Also determine if they often overlap something else or not.
 *
 * UTR is calculated as the locus set: UTR = exons - CDS
 * The 5' and 3' ends depend on the strand
 *
 * Assumptions about input: All exons in each gene consecutive,
 * all regions from each transcript consecutive without anything in between.
 */
'''

import pdb
#pdb.set_trace()

current_transcript=None

annotation=grok.reader(input_annotation)

full_ann = annotations({'exonic_length':'integer', 'CDS_length':'integer', 'relative_UTR_length':'real'})
dummy = hash_store(full_ann, annotation)


exons=dummy.redblack_store()
cds=dummy.redblack_store()

utrs = dummy.writer(os.path.join(output_folder, "utrs.gff")) # GTF output is right now a bad idea, so using gff

print "Dummy regions", utrs.get_region()

attributes = ['gene_id', 'Source', 'gene_biotype', 'gene_name', 'transcript_id', 'transcript_name']

for reg in annotation:
    if reg['Source']!='protein_coding': # Concept of UTR doesn't make sense for other types of regions, e.g. "processed transcripts"
        continue

    if reg['transcript_id']!=current_transcript:
        # Find UTR
        utr=diffL(exons, cds)
        e=[r for r in exons]
        c=[r for r in cds]

        #print "Before diff, exons are", e
        #print "Before diff, CDS are", [r for r in cds]
        #print "Got diff region", [r for r in utr]
        # ['exon_number',  'gene_id', 'Source', 'gene_biotype', 'gene_name', 'Phase', 'transcript_id', 'transcript_name', 'Type']
        for r in utr:
            # Copy attributes because diffL loses them. FIXME?
            for key in attributes:
                r[key]=e[0][key]
            r['Type']="UTR"
            r['exonic_length']=sum([reg['length'] for reg in e])
            r['CDS_length']=sum([reg['length'] for reg in c])
            r['relative_UTR_length']=float(r['length'])/r['exonic_length']

            # print "Adding regions ", r
            utrs.add(r)

        current_transcript=reg['transcript_id']

        exons=dummy.redblack_store()
        cds=dummy.redblack_store()
        #if l:
        #    print "Keys", e[0].keys()
        #    break

    if reg['Type']=='exon':
        exons.add(reg)
    elif reg['Type']=='CDS':
        cds.add(reg)



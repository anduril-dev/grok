
#!/usr/bin/env python
import anduril
from anduril.arrayio import *
from anduril.args import *
args=anduril.args

import os, errno, time, optparse, logging, sys, re, collections, heapq, bisect, copy, pprint, random, math, itertools, csv, pdb, grok, numpy, scipy

'''
/* Analysis
 *
 * Produce an expectation matrix as per model.py for each gene without overlapping exons,
 * because handling those isn't implemented properly yet.
 *
 * We don't yet care about strandedness - it shouldn't have effect on the matrix before
 * we start mapping reads to the equivalence classes. Actually it does affect the exon
 * indexing of each transcript, because the exons end up in backward coordinate order.
 *
 * Saving the matrices is done with pytables in a HDF5 file database.
 */
'''

import pdb
import model
import utils
import numpy

import tables

h5file = tables.openFile(output_matrices, mode="w", title="Expectation matrices")
group_matrices = h5file.createGroup("/", 'matrices', 'Matrices')
group_eq = h5file.createGroup("/", 'eq_classes', 'Equivalence classes')
group_transcripts = h5file.createGroup("/", 'transcripts', 'Transcripts that participate in forming the equivalence classes as lists of exons.')

try:
    with open( input_modelInput, "r") as model_input:
        amount=-1 # Positive to terminate on Nth iteration
        for line in model_input.readlines():

            gene, strand, exons, transcripts=eval(line)

            if strand==-1:
                exons.reverse()
                transcripts=[[len(exons)-1-i for i in t] for t in transcripts]

            # FIXME: Should do something with consecutive classes!

            matrix, eq_classes, consecutive_eq_classes=model.gene_to_matrix(exons, transcripts)
            # Ensure we have a result, though this should likely never happen in production
            if matrix==None:
                print >> sys.stderr, "No matrix for", gene
                continue
             
            # Pad equivalence classes with -1 to a fixed length so they can be stored as an array
            # FIXME: Notice the strand gets messed up in here
            padded_classes=utils.pad_nested(eq_classes)
            padded_transcripts=utils.pad_nested(transcripts)

            h5file.createArray(group_matrices, gene, matrix, "Expectation matrix")
            h5file.createArray(group_eq, gene, numpy.array(padded_classes), "EQ classes")
            h5file.createArray(group_transcripts, gene, numpy.array(padded_transcripts), "Transcripts")

            amount-=1
            if not amount: break

            if (amount % 1000)==0:
                print >> sys.stderr, "Amount is ", amount

finally:
    h5file.close()




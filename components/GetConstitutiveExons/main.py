
#!/usr/bin/env python
import anduril
from anduril.arrayio import *
from anduril.args import *
args=anduril.args

import os, errno, time, optparse, logging, sys, re, collections, heapq, bisect, copy, pprint, random, math, itertools, csv, pdb, grok, numpy, scipy

import model

def doit(inf, outf):
    out=open(outf, "w")

    with open(inf, "r") as model_input:
        lines=0
        for line in model_input.readlines():
            lines+=1
            if lines%200==0:
                print "Lines done:",lines
                #break
            gene, chromosome, gene_extent, strand, exons, transcripts=eval(line)

            #if strand==-1:
            #    exons.reverse()
            #    transcripts=[[len(exons)-1-i for i in t] for t in transcripts]

            # Assumes exons are on the forward strand...
            consecutive_eq_classes, constitutive_exons=model.consecutive(gene, exons, transcripts)
            out.write("(%r,%r,%r,%r,%r,%r)\n"%(gene, chromosome, strand, consecutive_eq_classes, constitutive_exons, [exons[e] for e in constitutive_exons]))

    out.close()

def main():
    doit( "model_input.py", "constitutive_classes.py" )

if __name__=="__main__":
    doit(input_modelInput, output_modelInput)


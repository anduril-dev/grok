
#!/usr/bin/env python
import anduril
from anduril.arrayio import *
from anduril.args import *
args=anduril.args

import os, errno, time, optparse, logging, sys, re, collections, heapq, bisect, copy, pprint, random, math, itertools, csv, pdb, grok, numpy, scipy

def attr(name,value):
    return ("%s=%r;"%(name, value)).replace(" ", "") # IGV doesn't like spaces.

def doit(input_filename, output_filename, threshold=-1):
    ann=grok.annotations({'name':'string'})
    out=ann.writer(output_filename)
    out.write_text('''#gffTags\n''')

    with open(input_filename, "r") as model_input:
        lines=0
        for line in model_input.readlines():
            lines+=1
            if lines%1000==0:
                print "Lines done:",lines
                #break

            gene, chromosome, strand, continuous, constitutive, const_exons=eval(line)
            #print "\nGene",gene

            name_ann=attr("Name",gene) # The name field is shown in IGV - Name is shown in overview. IGV cuts everything at a space; don't use them.
            reg_small={'name': name_ann, 'seqid': chromosome, 'strand':strand }
            reg_big={'name': name_ann, 'seqid': chromosome, 'strand':strand }

            pairs=[set(c[0]) for c in continuous]
            areas=[ c[1][0] for c in continuous]
            lens=[sum( k[1]-k[0] for k in c[1]) for c in continuous]
            #print "Pairs", pairs, "Areas", areas, "Lens", lens

            #print "continuous", continuous

            const_lens=[c[1]-c[0] for c in const_exons]
            max_const=max(const_lens) if const_lens else 0 
            #print "Constitutive lengths", const_lens, "; maximum", max_const

            biggest=0

            for c in range(len(pairs)):
                s=pairs[c]

                #print s
                #print "\tSmalls"
                small=c
                for small in range(c, 0, -1): # Extend backward
                    if not s.issubset(pairs[small]):
                        small+=1
                        break

                total=sum(lens[small:c+1])
                biggest=max(total, biggest)
                if total>threshold:
                    reg_small['name'] = name_ann \
                                        + attr("highLow","%d,%d"%(c,small)) \
                                        + attr("EQ","%r,%r"%(tuple(pairs[small]), tuple(pairs[c])))
                    reg_small['left'] = areas[small][0]
                    reg_small['length'] = areas[c][1] - reg_small['left']
                    #print "\t", total, small, c, pairs[small], s, reg_small['left'], reg_small['left']+reg_small['length']
                    out.add(reg_small)

                #print "\tBigs"
                big=c
                for big in range(c, len(pairs)): # Extend forward
                    if not s.issubset(pairs[big]):
                        big-=1
                        break

                if not c==big: # This is handled already in the previous as c==small
                    total=sum(lens[c:big+1])
                    biggest=max(total, biggest)
                    if total>threshold:
                        reg_big['name'] = name_ann \
                                        + attr("lowHigh","%d,%d"%(c,big)) \
                                        + attr("EQ","%r,%r"%(tuple(pairs[c]),tuple(pairs[big])))
                        reg_big['left'] = areas[c][0]
                        reg_big['length'] = areas[big][1] - reg_big['left']
                        #print "\t", total, c, big, s, pairs[big], reg_big['left'], reg_big['left']+reg_big['length']
                        out.add(reg_big)

            if biggest<max_const:
                print "Something is wrong, total length is smaller than that from constitutive exons."
                print biggest, max_const
                out.close()
                sys.exit(1)

    out.close()

def main():
    # doit("constitutive_classes.py", "continuous_pairs.csv", threshold=1000 )
    doit("constitutive_classes.py", "continuous_pairs.bed" )

if __name__=="__main__":
    main()


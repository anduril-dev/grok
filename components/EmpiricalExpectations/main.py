
#!/usr/bin/env python
import anduril
from anduril.arrayio import *
from anduril.args import *
args=anduril.args

import os, errno, time, optparse, logging, sys, re, collections, heapq, bisect, copy, pprint, random, math, itertools, csv, pdb, grok, numpy, scipy

'''
/* Analysis
 *
 * Same as "expectation_matrices_py" above but feeds in an empirical insert size
 * distribution, that will be used for the matrices.
 *
 * Doesn't output the additional information produced in the previous version,
 * only the matrices.
 */
'''

import pdb
import model
import utils
import numpy

from numpy import linalg
import tables

# Obtain insert size distribution
h5_insert_pdf = tables.openFile(input_insertPDF, mode="r")

# List all arrays - should contain "insert_sizes" and "insert_size_distribution"
h5_arrays={}
for group in h5_insert_pdf.walkGroups("/"):
    for array in h5_insert_pdf.listNodes(group, classname='Array'):
        h5_arrays[array.name]=array.read()

insert_pdf=h5_arrays["insert_size_distribution"]

# Output file
h5file = tables.openFile(output_matrices, mode="w", title="Expectation matrices")
group_matrices = h5file.createGroup("/", 'matrices', 'Matrices')
group_inverse = h5file.createGroup("/", 'inverse', 'Pseudo-inverse matrices')
try:
    with open(input_modelInput, "r") as model_input:
        amount=-1 # Positive to terminate on Nth iteration
        for line in model_input.readlines():

            gene, chromosome, strand, exons, transcripts=eval(line)

            #if strand==-1L:
            #    exons.reverse()
            #    transcripts=[[len(exons)-1-i for i in t] for t in transcripts]
            #    print "Reversed exons"

            matrix, eq_classes=model.empirical_gene_to_matrix(gene, exons, transcripts, insert_pdf)
            
            # Ensure we have a result, though this should likely never happen in production
            if matrix==None or not matrix.size:
                print >> sys.stderr, "No matrix for", gene
                continue

            # print "Empirical transcripts/exons", transcripts, exons
            # mat_s=("%s"%matrix).split("\n")
            # for idx in range(len(mat_s)):
            #     print "%s\t%s"%(pad_left("%r"%(list(eq_classes)[idx],), 14, " ") , mat_s[idx])
            # print "="
            # print numpy.sum(matrix, axis=0)

            #print group_matrices, gene, matrix
            #print group_inverse, gene, matrix

            h5file.createArray(group_matrices, gene, matrix, "Expectation matrix")

            # Compute the (Moore-Penrose) pseudo-inverse of a matrix and store it
            inverse = linalg.pinv(matrix)

            h5file.createArray(group_inverse, gene, inverse, "Pseudo-inverse matrix")

            amount-=1
            if not amount: break

            if (amount % 1000)==0:
                print >> sys.stderr, "Amount is ", amount

finally:
    h5file.close()



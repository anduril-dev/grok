
#!/usr/bin/env python
import anduril
from anduril.arrayio import *
from anduril.args import *
args=anduril.args

import os, errno, time, optparse, logging, sys, re, collections, heapq, bisect, copy, pprint, random, math, itertools, csv, pdb, grok, numpy, scipy

'''
/* Analysis
 *
 * Produce *parameters* that are needed to produce an expectation matrix as per model.py
 * These parameters are to be used by the expectation_matrices_py snippet below.
 * Currently a gene's exons and transcripts' exons indexed on those are written in a text file,
 * as a python tuple.
 *
 * Because genes are present on multiple strands, two strands are read simultaneously in one pass.
 *
 * This script will fail if two genes on the same strand overlap in genomic coordinates - it could
 * be improved to handle that case too.
 *
 * Some genes overlap in genomic coordinates, which should be accounted for in later analysis.
 *
 * Example output row:
 *  (
        'ENSG00000261603',
        -1L,
        [(46761072L, 46761493L), (46775125L, 46775385L), (46775554L, 46775732L), (46777796L, 46777919L)],
        [[0, 1, 2, 3]]
    )
 */
'''
import pdb
import model
import itertools
from itertools import * 

#pdb.set_trace()

# Input that can be used with model.py, having transcripts with their exons expressed as tersely as possible
python_test_input=open(output_modelInput, "w")

# Build a list of strand, transcripts and exons for each gene, and exons for each transcript
gene_strand={}
gene_transcripts={}
gene_exons={}
gene_chromosome={}

transcript_exons={}

annotation=grok.reader(input_annotation)
for reg in annotation:
    if reg['Source']!='protein_coding':
        continue

    gene=reg['gene_id']
    gene_strand[gene]=reg['strand']
    gene_chromosome[gene]=reg['seqid']

    transcript=reg['transcript_id']

    if reg['Type']=='exon':
        exon_tuple=(reg['left'], reg['right'])

        if not gene in gene_exons:
            gene_exons[gene]=set()
            gene_transcripts[gene]=set()

        gene_exons[gene].add(exon_tuple)
        gene_transcripts[gene].add(transcript)

        if not transcript in transcript_exons:
            transcript_exons[transcript]=[]

        transcript_exons[transcript].append(exon_tuple)

for gene in gene_strand.keys():

    exon_list=sorted(list(gene_exons[gene]), reverse=(gene_strand==-1L))

    #print "Strand is", reg['strand']
    #print transcript_exons
    #print "Gene exons", exon_list
    #print "\n"
    transcripts=[[exon_list.index(e) for e in exon_list if e in t_exons] for t_exons in [transcript_exons[transcript] for transcript in gene_transcripts[gene]] ]
    python_test_input.write("(%r, %r, %r, %r, %r)\n"%(gene, gene_chromosome[gene], gene_strand[gene], exon_list, transcripts))

python_test_input.close()


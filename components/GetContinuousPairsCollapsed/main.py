
#!/usr/bin/env python
import anduril
from anduril.arrayio import *
from anduril.args import *
args=anduril.args

import os, errno, time, optparse, logging, sys, re, collections, heapq, bisect, copy, pprint, random, math, itertools, csv, pdb, grok, numpy, scipy

def attr(name,value):
    return ("%s=%r;"%(name, value)).replace(" ", "") # IGV doesn't like spaces.

def doit(ifn, ofn, ofn_bam, ofn_gff, threshold=-1):
    annBam=grok.annotations({'name': 'string'})
    bamOut=annBam.writer(ofn_bam, "bed")
    gffOut=annBam.writer(ofn_gff, "gff")

    ann=grok.annotations({'name':'string'})
    out=ann.writer(ofn, "bed")
    out.write_text('''#gffTags\n''')

    with open(ifn, "r") as model_input:
        lines=0
        for line in model_input.readlines():
            lines+=1
            if lines%1000==0:
                print "Lines done:",lines
                #break

            gene, chromosome, strand, continuous, constitutive, const_exons=eval(line)
            #print(gene, chromosome, strand, continuous, constitutive, const_exons)

            name_ann=attr("Name",gene) # The name field is shown in IGV - Name is shown in overview. IGV cuts everything at a space; don't use them.
            reg={'name': name_ann, 'seqid': chromosome, 'strand':strand }

            pairs=[set(c[0]) for c in continuous]
            areas=[ c[1][0] for c in continuous]
            lens=[sum( k[1]-k[0] for k in c[1]) for c in continuous]
            #print "Pairs", pairs, "Areas", areas, "Lens", lens

            #print "continuous", continuous

            const_lens=[c[1]-c[0] for c in const_exons]
            max_const=max(const_lens) if const_lens else 0 
            #print "Constitutive lengths", const_lens, "; maximum", max_const

            biggest=0

            # Find ascending monotonous EQ
            sole=[]
            produced=[] # Don't output already covered intervals - This only works in one direction now, so there are going to be areas covered twice. It doesn't matter for the purpose of insert size distribution estimation though.
            c=len(pairs)-1
            while c>-1:
                s=pairs[c]
                #print s
                #print "\tSmalls"
                small=c
                for small in range(c, 0, -1): # Extend backward
                    if not s.issubset(pairs[small]):
                        small+=1
                        break
                    s=pairs[small]

                if c==small:
                    sole.append(c)
                else:
                    total=sum(lens[small:c+1])
                    biggest=max(total, biggest)
                    if total>threshold:
                        reg['name'] = name_ann \
                                + attr("highLow","%d,%d/%d"%(c,small,len(pairs))) \
                                + attr("EQ","%r,%r"%(tuple(pairs[c]), tuple(pairs[small]))) \
                                + attr("ALL","%r"%([tuple(p) for p in pairs]))
                        reg['left'] = areas[small][0]
                        reg['length'] = areas[c][1] - reg['left']
                        #print "\t", total, small, c, pairs[small], s, reg['left'], reg['left']+reg['length']
                        produced.append(set(range(small,c+1)))
                        out.add(reg)
                        reg['name'] = gene
                        bamOut.add(reg)
                        gffOut.add(reg)
                c=small-1

            # Find descending monotonous EQ
            c=0
            while c<len(pairs):
                s=pairs[c]
                #print "\tBigs"
                big=c
                for big in range(c, len(pairs)): # Extend forward
                    if not s.issubset(pairs[big]):
                        big-=1
                        break
                    s=pairs[big]

                if (not c==big) or c in sole: # This is handled already in the previous as c==small
                    cbig=set(range(c,big+1))
                    contained=[ cbig.issubset(p) for p in produced ]
                    if True in contained:
                        pass
                    else:
                        total=sum(lens[c:big+1])
                        biggest=max(total, biggest)
                        if total>threshold:
                            reg['name'] = name_ann \
                                + attr("lowHigh","%d,%d/%d"%(c,big,len(pairs))) \
                                + attr("EQ","%r,%r"%(tuple(pairs[c]),tuple(pairs[big]))) \
                                + attr("ALL","%r"%([tuple(p) for p in pairs])) \
                                + attr("produced","%r"%([tuple(p) for p in produced]))
                            reg['left'] = areas[c][0]
                            reg['length'] = areas[big][1] - reg['left']
                            #print "\t", total, c, big, s, pairs[big], reg['left'], reg['left']+reg['length']
                            out.add(reg)
                            reg['name'] = gene
                            bamOut.add(reg)
                            gffOut.add(reg)
                c=big+1

            if biggest<max_const:
                print "Something is wrong, total length is smaller than that from constitutive exons."
                print biggest, max_const
                out.close()
                sys.exit(1)

    out.close()

#def main():
# doit("constitutive_classes.py", "continuous_pairs.csv", threshold=1000 )
#    doit("constitutive_classes.py", "continuous_pairs_collapsed.bed" )

if __name__=="__main__":
    doit(input_modelInput, output_igvBed, output_bed, output_gff, param_threshold)


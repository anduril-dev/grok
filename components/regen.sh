#!/bin/bash

filename=$(basename "$1")
extension="${filename##*.}"
filename="${filename%.*}"

script_to_anduril.py $1 ; anduril run-component -b ~/anduril-bundles/sequencing/ -b ~/anduril-bundles/grok/ -L $filename >> $filename/example.and



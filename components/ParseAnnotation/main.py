
#!/usr/bin/env python
import anduril
from anduril.arrayio import *
from anduril.args import *
args=anduril.args

import os, errno, time, optparse, logging, sys, re, collections, heapq, bisect, copy, pprint, random, math, itertools, csv, pdb, grok, numpy, scipy

import pdb
import model
import itertools
from itertools import * 

def extract_genes(annotation, outf, stranded=False):
    #pdb.set_trace()

    # Input that can be used with model.py, having transcripts with their exons expressed as tersely as possible
    python_test_input=open(outf, "w")

    # Build dicts containing various facts about each gene and the transcripts
    gene_strand={}
    gene_transcripts={}
    gene_exons={}
    gene_chromosome={}

    transcript_exons={}

    processed=0
    for reg in annotation:
        processed+=1
        if processed %100000 == 0:
            print "Processed", processed
            #break

        if reg['Source']!='protein_coding':
            continue

        gene=reg['gene_id']
        gene_strand[gene]=reg['strand']
        gene_chromosome[gene]=reg['seqid']

        transcript=reg['transcript_id']

        if reg['Type']=='exon':
            exon_tuple=(reg['left'], reg['right'])

            if not gene in gene_exons:
                gene_exons[gene]=set()
                gene_transcripts[gene]=set()

            gene_exons[gene].add(exon_tuple)
            gene_transcripts[gene].add(transcript)

            if not transcript in transcript_exons:
                transcript_exons[transcript]=[]

            transcript_exons[transcript].append(exon_tuple)

    gene_extents={}
    # Calculate gene extents
    for gene in gene_strand.keys():
        exon_list=sorted(list(gene_exons[gene]), reverse=(gene_strand==-1L))
        first=exon_list[0]
        last=exon_list[-1]
        if first[0]>last[0]:
            print "Exons in wrong order for gene", gene, exon_list
            sys.exit(1)
            break
        gene_extent=(min(first), max(last))
        gene_extents[gene]=gene_extent
        if gene_extent[1] < gene_extent[0]:
            print "Gene extent doesn't make sense!"
            sys.exit(1)
            break

    # Strand is considered during sorting because reads from one strand aren't supposedly confused with the other strand
    # This should be confirmed
    convert = lambda text: int(text) if text.isdigit() else text.lower() 
    if stranded:
        genes_sorted = sorted(gene_strand.keys(), key=lambda x: (convert(gene_chromosome[x]), gene_strand[gene], gene_extents[x][0]) )
    else:
        genes_sorted = sorted(gene_strand.keys(), key=lambda x: (convert(gene_chromosome[x]), gene_extents[x][0]) )
    for gene in genes_sorted:
        gene_extent=gene_extents[gene]
        exon_list=sorted(list(gene_exons[gene]), reverse=(gene_strand==-1L))

        #print "Strand is", reg['strand']
        #print transcript_exons
        #print "Gene exons", exon_list
        #print "\n"
        transcripts=[[exon_list.index(e) for e in exon_list if e in t_exons] for t_exons in [transcript_exons[transcript] for transcript in gene_transcripts[gene]] ]
        python_test_input.write("(%r, %r, %r, %r, %r, %r)\n"%(gene, gene_chromosome[gene], gene_extent, gene_strand[gene], exon_list, transcripts))

    python_test_input.close()

def join_genes(inf, outf):
    genes=open(inf, 'r')
    sharing=open(outf, 'w')

    class cur: # Just for syntax sugar
        seqid=None
        strand=None
        low=None
        genes=[] # Currently accumulated gene list

    reach=-1 # How deep are we into the current positions
    for g in genes.readlines():
        g=eval(g)
        if cur.seqid!=g[1]:
            cur.seqid=g[1]
            reach=-1
        if cur.strand!=g[3]:
            cur.strand=g[3]
            reach=-1

        low,high=g[2]
        if low<reach:
            cur.genes.append(g[0])
            reach=max(high,reach)
        else:
            #sharing.write("%r\n"%((tuple(cur.genes), cur.low, reach, g), ))
            if cur.genes:
                sharing.write("%r\n"%(tuple(cur.genes), ))
            cur.genes=[g[0]]
            cur.low=low
            reach=high
    #sharing.write("%r\n"%((tuple(cur.genes), cur.low, reach, g), ))
    sharing.write("%r\n"%(tuple(cur.genes), ))

def main():
    # Could verify input is a GTF file
    if len(sys.argv)<2:
        print "Provide a GTF file."
        sys.exit(1)
    annotation=grok.reader(sys.argv[1])
    if True and os.path.exists("model_input.py"):
        print "model_input.py already exists, skipping."
    else:
        extract_genes(annotation, "model_input.py")

    if False and os.path.exists("sharing.py"):
        print "sharing.py already exists, skipping."
    else:
        join_genes("model_input.py", "sharing.py")

if __name__=="__main__":
    annotation=grok.reader(input_annotation)
    extract_genes(annotation, output_modelInput)
    join_genes(output_modelInput, output_sharing)


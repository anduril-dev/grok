
#!/usr/bin/env python
import anduril
from anduril.arrayio import *
from anduril.args import *
args=anduril.args

import os, errno, time, optparse, logging, sys, re, collections, heapq, bisect, copy, pprint, random, math, itertools, csv, pdb, grok, numpy, scipy

'''
/* Preprocessing - UNUSED AS NEEDLESS
 * This can be done wherever its required as it doesn't take too long.
 * Create BAM files with all the genes on a chromosome as the sequence set, with zero regions.
 *
 * I would use one BAM file per gene unless that produced too many files.
 *
 * => GROK has a global sequence set, so all the bam files end up identical.
 * Either somehow fix that, or just use the "all_genes.bam" file produced.
 *
 * So, this snippet is not really used, because it can as well be done in each
 * component requiring such a set.
 */
 '''

genes=input_annotation

writers={}
all_genes=hash_store()

for g in genes:
    value=g["seqid"] # The chromosome
    if not value in writers:
        writers[value]=hash_store()
    g["seqid"]=g["ID"]
    writers[value].add(g)
    all_genes.add(g)

for w in writers.keys():
    writers[w].writer(os.path.join(output_folder, w+".bam"))
all_genes.writer(os.path.join(output_folder, "all_genes.bam"))



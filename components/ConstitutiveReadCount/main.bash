
#!/bin/bash
# running functions.sh also sets logfile and errorfile.
source "$ANDURIL_HOME/bash/functions.sh"
export_command
tempdir=$( gettempdir ) # nice to have it always since it's the scratch disk

#// pe_utils.py is only installed on vm2, but is installable from gc-home/misopy-0.4.9
#// http://genes.mit.edu/burgelab/miso/docs/#id79
#// Also requires bedtools (>= 2.17), the tagBam program
#// FIXME: Should add bash_header_sh or set -e, doesn't fail when supposed to...

set -e
set -x

pe_utils.py --compute-insert-len $input_alignment $input_constExons --output-dir $output_folder


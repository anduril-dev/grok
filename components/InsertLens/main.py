
#!/usr/bin/env python
import anduril
from anduril.arrayio import *
from anduril.args import *
args=anduril.args

import os, errno, time, optparse, logging, sys, re, collections, heapq, bisect, copy, pprint, random, math, itertools, csv, pdb, grok, numpy, scipy

'''
/* Preprocessing
 *
 * Parse an insert length distribution produce by MISO's pe_utily.py and save it in HDF5 format.
 * Input is a the folder output of PEDistEstimator.
 */
 '''
import pdb
import numpy
import tables

# Could use the old HDF5 database but creating a new one for Anduril
h5file = tables.openFile(output_insertPDF, mode="w", title="Insert size distribution")
group = h5file.createGroup("/", 'distribution', 'Distribution')

try:
 # The name of this file depends!
 #alignment.bam.insert_len with the insert lengths
 # and alignment.bam, presumably with the actual hits to those exons.
 #
 # The format is:
 # chromosome:start-end:strand\tlengths,comma,separated
 # 17:79430637-79433357:+  225,123,...
    #length_file=os.path.join(var1, "alignment.bam.insert_len")
    length_file=[ f for f in os.listdir(input_MISOresults) if f.endswith("insert_len")] [0]
    length_file=os.path.join(input_MISOresults, length_file)

    result=numpy.zeros(1000)

    with open(length_file, "r") as lengths:
        amount=-1 # Positive to terminate on Nth iteration, -1 to ignore

        for line in lengths:
            if line[0]=="#": continue # Skip comments

            area,nums=line.split("\t")
            chromo,ival,strand=area.split(":")

            strand=(0 if strand=="+" else 1) # Not used yet!

            nums=nums.split(",")
            x = numpy.array(nums).astype(numpy.int)
            x = numpy.bincount(x)
            x.resize(result.shape)
            result+=x

            #    print >> sys.stderr, "No matrix for", gene
             
            amount-=1
            if not amount: break

            if (amount % 1000)==0:
                print >> sys.stderr, "Amount is ", amount

        empirical_pdf=result.astype(numpy.double)/result.astype(numpy.double).sum()

        h5file.createArray(group, "insert_sizes", result, "Insert sizes corresponding to the distribution")
        h5file.createArray(group, "insert_size_distribution", empirical_pdf, "Insert size distribution")

finally:
    h5file.close()




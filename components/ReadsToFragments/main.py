
#!/usr/bin/env python
import anduril
from anduril.arrayio import *
from anduril.args import *
args=anduril.args

import os, errno, time, optparse, logging, sys, re, collections, heapq, bisect, copy, pprint, random, math, itertools, csv, pdb, grok, numpy, scipy

'''
/* Preprocessing
 *
 * We are only interested in rows that have a corresponding mate row with a negative template
 * length, BAM's tlen field.
 *
 * That means both the read and its mate were mapped successfully to the same chromosome.
 *
 * The read position can later be deduced from the mate and the template length as
 * read position = mate position + mate length - template length
 * 
 * In principle, should check the flag 0x40 for "first segment" i.e. read and 0x80 for "last segment" i.e. mate. This is done with the 0x40/0x80 flag checks.
 * FIXME: Still check if that m_left =r_pnext holds true even if read/mate are reversed
*/
'''

import itertools
from itertools import * 

sorted=grok.reader(input_sorted)
fragments=sorted.writer(os.path.join(output_fragments), "bam")

#outfile=open("/home/llyly/bamsam.regs", "w")

produced=0
def produce_fragment(reg1, reg2):
    global fragments
    global produced
    #global outfile
    # FIXME: Detect unpaired
    # Swap read and mate once - if they are still not ordered, throw
    if reg1['tlen']<0:
        d=reg1
        reg1=reg2
        reg2=d
        if reg1['tlen']<0:
            raise Exception("Mates in unexpected order " + str(reg1) + "\n" + str(reg2))

    # Switch order
    if reg1['left'] > reg2['left']:
        d=reg1
        reg1=reg2
        reg2=d

    #print reg1, "\n", reg2
    reg1['pnext']=0
    reg1['rnext']=0
    #reg1['Sequence']+=reg2['Sequence']
    #reg1['Phred']+=reg2['Phred']
    reg1['Sequence']=None
    reg1['Phred']=None
    # This would only work for non-standard SAM from tophat-fusion, never for BAM: reg1['CIGAR']=reg1['CIGAR']+'F'+str(reg2['left'])+reg2['CIGAR']

    # The length in BAM is deduced from CIGAR so write it there. (No separate field for it)
    reg1['length']=reg2['right']-reg1['left']
    reg1['right']=reg1['left']+reg1['length']
    #reg1['CIGAR']=reg1['CIGAR'] + str(reg2['left'] - reg1['left'] - reg1['length']) + 'N' + reg2['CIGAR']
    #reg1['length']=reg1['tlen']
    reg1['CIGAR']=str(reg1['length'])+'M'
    reg1['n_cigar']=1 # Must match CIGAR length!
    
    #print reg1, reg2
    #print "Adding", reg1
    produced+=1
    #outfile.write("%r\n"%reg1)
    fragments.add(reg1)

ID=None
reads=[]
mates=[]

total_regions=0
total_paired=0

perceived_reads=0
perceived_mates=0

double_mates=0
# Adding reads is triggered when the region ID is switched
# The dummy region is to stop the loop gracefully

# JoinMates
for region in chain(sorted,[{'ID':None, 'tlen':0}]):
    total_regions+=1
    region['Tags']=None # These may cause problems at the moment
    if not region['ID']==ID:

        # Try to match each mate
        for m in mates:
            added=False
            for r in reads:
                if r['tlen']==-m['tlen'] and m['left'] == r['pnext']-1 and (r['flag']&0x40) == (m['flag'] & 0x80):
                    if added:
                    #    raise Exception("More than one read for a mate! %r"%((reads,m), ))
                    #     print "More than one read for a mate! %r"%((reads,m), )
                        #paired.add(m) # Add mate already here so we get pairwise data
                        produce_fragment(r,m)
                        total_paired+=1
                        double_mates+=1
                    total_paired+=1
                    #paired.add(r) 
                    added=True

#                if not added:
#                    raise Exception("Could not pair a mate!")
#                    print "Could not pair a mate!"
#        print "Added!"
            if added:    
                total_paired+=1
                #paired.add(m)
                produce_fragment(r,m)

        mates=[]
        reads=[]
        ID=region["ID"]
    else:
        #print "Same ID"
        pass

    # In BAM files, the rightmost segment in a template has a negative template length
    if region['tlen']<0:
        perceived_mates+=1
        mates.append(region)
    else:
        perceived_reads+=1
        reads.append(region)



print "Number of double mates", double_mates
print "Total reads", total_regions
print "Total in paired", total_paired
print "Percentage", total_paired/float(total_regions)
print "Perceived reads, mates", perceived_reads, perceived_mates
print "Produced", produced

fragments.close()
#outfile.close()


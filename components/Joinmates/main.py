
#!/usr/bin/env python
import anduril
from anduril.arrayio import *
from anduril.args import *
args=anduril.args

import os, errno, time, optparse, logging, sys, re, collections, heapq, bisect, copy, pprint, random, math, itertools, csv, pdb, grok, numpy, scipy

'''
/* Preprocessing
 *
 * From the preceding mate-paired reads, create regions that represent the fragments' ends
 * on the genome.
 *
 * Note: Could be merged with the previous step.
 */
'''
mates = grok.reader(input_mates, "bam")
fragments = mates.writer(os.path.join(output_joinedPairs), "bam") # For some reason, writing a bam file like this is tricky, so just use bed instead. Should be gzip'd.

reg1=None
reg2=None

paired=False

# Detect unpaired
for m in mates: # Check that IDs match before attempting to join! Are they ID sorted to begin with, which is probably what this code assumes?
    if reg1 and reg2:
        # Swap read and mate once - if they are still not ordered, throw
        if reg1['tlen']<0:
            d=reg1
            reg1=reg2
            reg2=d
            if reg1['tlen']<0:
                raise Exception("Mates in unexpected order " + str(reg1) + "\n" + str(reg2))

        # Switch order
        if reg1['left'] > reg2['left']:
            d=reg1
            reg1=reg2
            reg2=d

        #print reg1, "\n", reg2
        reg1['pnext']=0
        reg1['rnext']=0
        #reg1['Sequence']+=reg2['Sequence']
        #reg1['Phred']+=reg2['Phred']
        reg1['Sequence']=None
        reg1['Phred']=None
        # This would only work for non-standard SAM from tophat-fusion, never for BAM: reg1['CIGAR']=reg1['CIGAR']+'F'+str(reg2['left'])+reg2['CIGAR']

        # The length in BAM is deduced from CIGAR so write it there. (No separate field for it)
        reg1['length']=reg2['right']-reg1['left']
        reg1['right']=reg1['left']+reg1['length']
        #reg1['CIGAR']=reg1['CIGAR'] + str(reg2['left'] - reg1['left'] - reg1['length']) + 'N' + reg2['CIGAR']
        #reg1['length']=reg1['tlen']
        reg1['CIGAR']=str(reg1['length'])+'M'
        
        #print reg1, reg2
        #print "Adding", reg1
        fragments.add(reg1)
        reg1=None
        reg2=None
    else:
        if not reg1:
            reg1=m
        else:
            reg2=m

fragments.close()


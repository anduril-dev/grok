
#!/usr/bin/env python
import anduril
from anduril.arrayio import *
from anduril.args import *
args=anduril.args

import os, errno, time, optparse, logging, sys, re, collections, heapq, bisect, copy, pprint, random, math, itertools, csv, pdb, grok, numpy, scipy

'''
/* Analysis
 *
 * For each gene, count reads to the equivalence classes as per "model.py". This produces
 * one vector per gene which corresponds to an observation for that gene, i.e. a discrete
 * distribution over the gene's fragment space.
 *
 * The analysis is centered on a set of target genes, expressed in the "model_input" format
 * used previously, because that's the most concise format expressing a gene's transcripts,
 * and easy to use from Python.
 *
 * As such a list is maximally only maybe 20000 lines long, it can be parsed into memory
 * easily and fast.
 */
'''

import pdb
import model
import grok
#pdb.set_trace()

# Create a reader for the aligned reads, assumed to be a BAM file and sorted in genomic order
db = grok.reader(input_sortedAlignment)
model_input=open(input_modelInput, "r")

equivalence_counts=open(output_counts, "w")

missed=db.writer(output_missed, "bam")

def group_writer(region_set, group, reg):
    reg["Tags"]="RG:Z:%r"%(group)
    #print "Group writer, adding tag ", reg["Tags"]
    #print reg
    reg['Sequence']=None # Works around an apparent bug!
    reg['Phred']=None
    region_set.add(reg)
writer_missed=lambda reg: group_writer(missed, 1, reg)
writer_mapped=lambda reg: group_writer(missed, 2, reg)

# Access genes directly from the model input
# FIXME: Though, it would be possible to avoid the overlap if model input is also sorted! This is an optimization, postponing for now
for line in model_input.readlines():
    gene, chromosome, strand, exons, transcripts=eval(line)
    
    # Figure out minimum and maximum of the gene - exons assumed to be ordered
    start, end = exons[0][0], exons[-1][1]

    query_region = {"seqid":chromosome, "left":start, "length":end-start, "strand":strand}
    reads = db.overlap(query_region) # Not supported with BED...
    eq_classes,observation,total,mapped=model.reads_to_observation( gene, exons, transcripts, reads, writer_mapped, writer_missed )
    equivalence_counts.write("(%r,%r,%r)\n"%(gene,eq_classes,observation))

    if total:
        #print "Gene", gene, "Query", query_region, "Total", total, "mapped", mapped, "observation", observation
        #reads = db.overlap(query_region)
        pass

"""
Pseudocode:

for each gene's overlap with the bam file:
    params=model_input
    observation=model.reads_to_observation(gene, exons, transcripts, reads)
    expression=h5matrix(gene) * observation
    output to CSV
    
"""

model_input.close()
equivalence_counts.close()
missed.close()



#!/usr/bin/env python
import anduril
from anduril.arrayio import *
from anduril.args import *
args=anduril.args

import os, errno, time, optparse, logging, sys, re, collections, heapq, bisect, copy, pprint, random, math, itertools, csv, pdb, grok, numpy, scipy

'''
/* Preprocessing
 *
 * Create one bam file per chromosome with genes annotated. This means we need all the genes in a sequence set.
 */
 '''
import utils
from utils import *

paired = grok.reader(input_paired)

for n in paired:
    sample=n
    break

genes = grok.reader(input_genes)

# Add genes to sequence set - the sequence set is global.
all_genes=hash_store()
for g in genes:
    g["seqid"]=g["ID"]
    all_genes.add(g)

writers={}

for g in genes:
    if g["ID"]=="ENSG00000143549":
        print "Found!"
    else:
        continue
    overlapping = paired.overlap(g)
    print "Checking overlap between", g, "and", sample, "overlap is", overlapping
    for reg in overlapping:
        trace("Overlapping")
        if not (reg['left']>=g['left'] and reg['right']<=g['right']):
            #raise Exception("Gene doesn't contain fragment fully.")
            continue

        value=g["seqid"] # The chromosome
        if not value in writers:
            writers[value]=paired.writer(os.path.join( output_folder , value + ".bam"))
        writer=writers[value]

        reg['Sequence']=None
        reg['Phred']=None

        reg['seqid']=g['ID']
        writer.add(reg)


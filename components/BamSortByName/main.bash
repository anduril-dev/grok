
#!/bin/bash
# running functions.sh also sets logfile and errorfile.
source "$ANDURIL_HOME/bash/functions.sh"
export_command
tempdir=$( gettempdir ) # nice to have it always since it's the scratch disk

set -x
set -e

# Preprocessing - Simple snippet to sort a bam file

# To keep the file extension and thus type available, the output must be a folder or an array

# Triggers failure if array isn't written to

echo "Sorting by name with samtools."

# For BAM
cd $(dirname $output_sorted )
samtools sort -n $input_alignment sorted

# For SAM
# samtools view -uS @var1@ | samtools sort - sorted



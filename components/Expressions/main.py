
#!/usr/bin/env python
import anduril
from anduril.arrayio import *
from anduril.args import *
args=anduril.args

import os, errno, time, optparse, logging, sys, re, collections, heapq, bisect, copy, pprint, random, math, itertools, csv, pdb, grok, numpy, scipy

'''
/* Analysis
 *
 * Performs matrix multiplication on the observed read counts by the inverses of the equivalence class mixing matrices. Result is written to a CSV file.
 */
'''

import pdb
import model
import utils
import numpy

from numpy import linalg
import tables

# Matrices
matrices = tables.openFile(input_matrices, mode="r")

h5file = tables.openFile(output_expressions, mode="w", title="Expression levels")
group_expressions = h5file.createGroup("/", 'expressions', 'Expression vectors')

with open(input_observation, "r") as model_input:
    amount=-1 # Positive to terminate on Nth iteration
    for line in model_input.readlines():
        gene, eq_classes, observation=eval(line)
        
        #// Matrix order
        #self.class_fractions=[ [0.]*len(self.transcripts) for i in range(len(self.eq_classes)) ]
        #// Sample observations
        #for key in self.eq_classes: # Initialize to zero
        #observation[key]=0
        #observation[tuple(sorted(map_result))]+=1
        #// from eq class
        observation=numpy.matrix([observation[c] for c in eq_classes]).transpose()
        try:
            inv = matrices.getNode("/inverse", gene) # Could also iterate, if order is same
            #print "Actually found gene %s "%(gene)
            result = numpy.transpose(inv * observation)
            h5file.createArray(group_expressions, gene, result, "Unnormalized expression")

            #print "Result for gene", gene, result
        except Exception as e:
            print >> sys.stderr, "No gene %s "%(gene), "or", e


        amount-=1
        if not amount: break

        if (amount % 1000)==0:
            print >> sys.stderr, "Amount is ", amount

